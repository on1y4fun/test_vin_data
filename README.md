# VIN number decoder

## Description

Using this service you can decode VIN number
and get vehicle information.

## How to launch

Clone repository of project

```
git clone https://gitlab.com/on1y4fun/test_vin_data.git

```
Launch application in docker containers

```
cd test_vin_data/infra/
```
```
docker-compose up -d --build
```

Implement migrations:

```
docker-compose exec vin_backend python manage.py migrate
```

Create superuser:

```
docker-compose exec vin_backend python manage.py createsuperuser
```

### API Endpoints

```api/v1/vehicles/ (POST)```: get data about vehicle using 
provided VIN number

### Request example

POST /api/v1/vehicles/ HTTP/1.1 <br />
Host: 127.0.0.1:8000 <br />
Content-Type: application/json <br />
Content-Length: 36 <br />

{
    "vin": "1P3EW65F4VV300946"
}

