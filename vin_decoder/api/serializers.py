from rest_framework import serializers

from vehicles.models import (
    Part,
    Vehicle,
    VehiclePart,
    VehicleWeight,
    WeightType,
)


class PartSerializer(serializers.ModelSerializer):
    vehicle_part = serializers.CharField(validators=[])

    class Meta:
        model = Part
        fields = ('vehicle_part',)

    def to_internal_value(self, data):
        return_data = super().to_internal_value(data)
        return_data['dimension_value'] = data['dimension_value']
        return return_data


class WeightTypeSerializer(serializers.ModelSerializer):
    type = serializers.CharField(validators=[])

    class Meta:
        model = WeightType
        fields = ('type',)

    def to_internal_value(self, data):
        return_data = super().to_internal_value(data)
        return_data['unit'] = data['unit']
        return_data['value'] = data['value']
        return return_data


class VehicleSerializer(serializers.ModelSerializer):
    dimensions = PartSerializer(many=True)
    weight = WeightTypeSerializer(many=True)

    class Meta:
        model = Vehicle
        fields = '__all__'

    def _weight_type_parser(self, weight_data, data_list, vehicle):
        """Parses vehicle weight information and prepares 
        data to related model"""
        weight_value = weight_data['value']
        weight_unit = weight_data['unit']
        current_weight_type, status = WeightType.objects.get_or_create(
            type=weight_data['type']
        )
        data_list.append(
            VehicleWeight(
                weight=current_weight_type,
                vehicle=vehicle,
                weight_value=weight_value,
                unit=weight_unit,
            )
        )
        return data_list

    def create(self, validated_data):
        dimensions = validated_data.pop('dimensions')
        weight_types = validated_data.pop('weight')
        vehicle = Vehicle.objects.create(**validated_data)
        vehicle_dimensions = []
        vehicle_weight = []
        for part in dimensions:
            dimension_value = part['dimension_value']
            current_part, status = Part.objects.get_or_create(
                vehicle_part=part['vehicle_part']
            )
            vehicle_dimensions.append(
                VehiclePart(
                    part=current_part,
                    vehicle=vehicle,
                    dimension_value=dimension_value,
                )
            )
        if isinstance(weight_types, dict):
            self._weight_type_parser(weight_types, vehicle_weight, vehicle)
        else:
            for weight in weight_types:
                self._weight_type_parser(weight, vehicle_weight, vehicle)
        VehiclePart.objects.bulk_create(vehicle_dimensions)
        VehicleWeight.objects.bulk_create(vehicle_weight)
        return vehicle
