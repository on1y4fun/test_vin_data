from rest_framework import status
from rest_framework.test import APITestCase
from vehicles.models import Vehicle

url = f'/api/v1/vehicles/'
data = {"vin": "1P3EW65F4VV300946"}


class AccountTests(APITestCase):
    def test_create_vehicle_instance(self):
        """Tests correct data saving with repeated request"""
        response = self.client.post(url, data, format='json')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Vehicle.objects.count(), 1)
        self.assertEqual(Vehicle.objects.get().vin_number, '1P3EW65F4VV300946')

    def test_check_response(self):
        response = self.client.post(url, data, format='json')
        response_data = {
            "year": "1997",
            "vin_number": "1P3EW65F4VV300946",
            "make": "Plymouth",
            "model": "Prowler",
            "type": "CONVERTIBLE 2-DR",
            "color": "Yellow",
            "dimensions": [
                {"vehicle_part": "Wheelbase", "dimension_value": "112.50"},
                {"vehicle_part": "Rear Legroom", "dimension_value": "39.50"},
                {"vehicle_part": "Front Legroom", "dimension_value": "41.20"},
                {"vehicle_part": "Overall Width", "dimension_value": "75.00"},
                {"vehicle_part": "Rear Headroom", "dimension_value": "36.00"},
                {"vehicle_part": "Rear Hip Room", "dimension_value": "54.70"},
                {"vehicle_part": "Front Headroom", "dimension_value": "38.40"},
                {"vehicle_part": "Front Hip Room", "dimension_value": "56.00"},
                {"vehicle_part": "Overall Height", "dimension_value": "66.00"},
                {
                    "vehicle_part": "Overall Length",
                    "dimension_value": "189.50",
                },
                {
                    "vehicle_part": "Rear Shoulder Room",
                    "dimension_value": "56.20",
                },
            ],
            "weight": [{"type": "Curb Weight", "unit": "lbs", "value": 5568}],
        }
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, response_data)
