from django.urls import path

from vehicles.views import VehicleView

urlpatterns = [
    path('v1/vehicles/', VehicleView.as_view()),
]
