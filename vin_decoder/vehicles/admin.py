from django.contrib import admin

from vehicles.models import (
    Part,
    Vehicle,
    VehiclePart,
    VehicleWeight,
    WeightType,
)


class PartInline(admin.TabularInline):
    model = VehiclePart


class WeightInline(admin.TabularInline):
    model = VehicleWeight


class VehicleAdmin(admin.ModelAdmin):
    list_display = (
        'pk',
        'vin_number',
        'make',
        'model',
        'year',
        'type',
        'color',
    )
    search_fields = ('model',)
    list_filter = ('model', 'color', 'year')
    empty_value_display = '-пусто-'
    inlines = (PartInline, WeightInline)


class PartAdmin(admin.ModelAdmin):
    list_display = (
        'pk',
        'vehicle_part',
    )
    search_fields = ('vehicle_part',)
    list_filter = ('vehicle_part',)
    empty_value_display = '-пусто-'


class WeightTypeAdmin(admin.ModelAdmin):
    list_display = (
        'pk',
        'type',
    )
    search_fields = ('type',)
    list_filter = ('type',)
    empty_value_display = '-пусто-'


admin.site.register(Vehicle, VehicleAdmin)
admin.site.register(Part, PartAdmin)
admin.site.register(WeightType, WeightTypeAdmin)
