from django.core.validators import MinValueValidator
from django.db import models


LARGE_LENGTH = 256
MEDIUM_LENGTH = 64


class Part(models.Model):
    vehicle_part = models.CharField(
        max_length=MEDIUM_LENGTH,
        unique=True,
    )

    class Meta:
        verbose_name = 'Part'
        verbose_name_plural = 'Parts'

    def __str__(self):
        return f'{self.vehicle_part}'


class WeightType(models.Model):
    type = models.CharField(
        max_length=MEDIUM_LENGTH,
        unique=True,
    )

    class Meta:
        verbose_name = 'Weight type'
        verbose_name_plural = 'Weight types'

    def __str__(self):
        return f'{self.type}'


class Vehicle(models.Model):
    vin_number = models.CharField(
        max_length=LARGE_LENGTH, verbose_name='VIN number', unique=True
    )
    make = models.CharField(max_length=LARGE_LENGTH, verbose_name='Maker name')
    model = models.CharField(
        max_length=LARGE_LENGTH, verbose_name='Model name'
    )
    year = models.IntegerField(verbose_name='Model year')
    type = models.CharField(
        max_length=LARGE_LENGTH,
        verbose_name='Model body type',
        blank=True,
        null=True,
    )
    color = models.CharField(
        max_length=MEDIUM_LENGTH,
        verbose_name='Vehicle color',
        blank=True,
        null=True,
    )
    dimensions = models.ManyToManyField(
        Part,
        through='VehiclePart',
        related_name='vehicle',
        verbose_name='Vehicle dimensions',
    )
    weight = models.ManyToManyField(
        WeightType,
        through='VehicleWeight',
        related_name='vehicle',
        verbose_name='Vehicle weight',
    )

    class Meta:
        verbose_name = 'Vehicle'
        verbose_name_plural = 'Vehicles'
        ordering = ('-year',)


class VehiclePart(models.Model):
    vehicle = models.ForeignKey(
        Vehicle,
        on_delete=models.SET_NULL,
        related_name='vehiclepart',
        null=True,
        verbose_name='Vehicle',
    )

    part = models.ForeignKey(
        Part,
        on_delete=models.SET_NULL,
        related_name='vehiclepart',
        null=True,
        verbose_name='Vehicle part',
    )

    dimension_value = models.FloatField(
        validators=[
            MinValueValidator(0.1),
        ],
        verbose_name='Dimension value',
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=('vehicle', 'part'), name='vehicle_part_model'
            )
        ]

    def __str__(self):
        return f'{self.vehicle} {self.part}'


class VehicleWeight(models.Model):
    vehicle = models.ForeignKey(
        Vehicle,
        on_delete=models.SET_NULL,
        related_name='vehicleweight',
        null=True,
        verbose_name='Vehicle',
    )

    weight = models.ForeignKey(
        WeightType,
        on_delete=models.SET_NULL,
        related_name='vehicleweight',
        null=True,
        verbose_name='Vehicle part',
    )

    weight_value = models.FloatField(
        validators=[
            MinValueValidator(1),
        ],
        verbose_name='Weight value',
    )

    unit = models.CharField(
        max_length=16,
        verbose_name='Weight unit',
    )

    def __str__(self):
        return f'{self.vehicle} {self.weight} {self.unit}'
