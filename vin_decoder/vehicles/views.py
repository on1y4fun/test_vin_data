from django.core import serializers
from rest_framework.views import APIView
from rest_framework.response import Response

import json
import requests

from vin_decoder.settings import DECODER_RESOURCE
from api.serializers import VehicleSerializer
from vehicles.models import Vehicle

to_present_data = (
    'year',
    'make',
    'model',
    'body',
    'color',
    'dimensions',
    'weight',
)


class VehicleView(APIView):
    def _parse_service_data(self, response_object, vin_number):
        """Parses decoding service data and gets information
        about vehicle. Brings data to correct view in order to serialize"""

        data = {}
        weight_list = []
        decode_info = response_object.json()['decode']
        if not decode_info['status'] == 'SUCCESS':
            data['messages'] = 'Could not decode number'
            return Response(data)
        vehicle_data = decode_info['vehicle'][0]
        for value in to_present_data:
            if value == 'weight' and isinstance(vehicle_data[value], dict):
                # Correct weight data to query
                weight_list.append(vehicle_data[value])
                data[value] = weight_list
            elif value == 'dimensions':
                # Rearrange parts and dimensions to serialize data
                dimensions = []
                for vehicle_part, dimension_value in vehicle_data[
                    value
                ].items():
                    parts_sizes = {}
                    parts_sizes['vehicle_part'] = vehicle_part
                    parts_sizes['dimension_value'] = dimension_value
                    dimensions.append(parts_sizes)
                data[value] = dimensions
            elif value == 'body':
                data['type'] = vehicle_data[value]
            else:
                data[value] = vehicle_data[value]
            data['vin_number'] = vin_number
        return data

    def _get_existing_data(self, vehicle):
        """Collects data from vehicle object and
        related models"""

        dimensions = []
        weight = []
        instance = serializers.serialize(
            'json',
            [
                vehicle,
            ],
        )
        json_object = json.loads(instance)[0]['fields']
        parts = vehicle.vehiclepart.values_list(
            'part__vehicle_part', 'dimension_value'
        )
        vehicle_weights = vehicle.vehicleweight.values_list(
            'weight__type', 'unit', 'weight_value'
        )
        for part_dimension in parts:
            dimension_values = {}
            dimension_values['vehicle_part'] = part_dimension[0]
            dimension_values['dimension_value'] = part_dimension[1]
            dimensions.append(dimension_values)
        for vehicle_weight in vehicle_weights:
            weight_values = {}
            weight_values['type'] = vehicle_weight[0]
            weight_values['unit'] = vehicle_weight[1]
            weight_values['weight_value'] = vehicle_weight[2]
            weight.append(weight_values)
        json_object['dimensions'] = dimensions
        json_object['weight'] = weight
        return json_object

    def post(self, request):
        """Handles POST request.

        VIN number in request data is checked if it is already in database.
        Otherwise implements request to decoding service and new data is saved.
        """
        vin_number = request.data['vin']
        vehicle = Vehicle.objects.filter(vin_number=vin_number)
        if vehicle:
            return Response(self._get_existing_data(vehicle.first()))
        resource_response_object = requests.get(
            f'{DECODER_RESOURCE}{vin_number}'
        )
        data = self._parse_service_data(resource_response_object, vin_number)
        serializer = VehicleSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
        return Response(data)
